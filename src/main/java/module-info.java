module com.example.guiforgroup {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.guiforgroup to javafx.fxml;
    exports com.example.guiforgroup;
}