package com.example.guiforgroup;

public enum CitationMethods {
    BookIEEE,
    BookAPA,
    BookChicago,
    BookHarvard,
    ArticleIEEE,
    ArticleAPA,
    ArticleChicago,
    ArticleHarvard
}
