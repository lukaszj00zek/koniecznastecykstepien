package com.example.guiforgroup;

public class CitationBook extends Book {
    protected String author;
    protected String doi;
    protected String title;
    protected String url;
    protected String year;
    protected String publisher;

    //Book's constructor based on the Source
    public CitationBook(String id, String author, String doi, String month, String pages, String title, String url, String year, String journal, String issn, String issue, String isbn,
                        String publisher, String volume, String author1, String doi1, String title1, String url1, String year1, String publisher1) {
        super(id, author, doi, month, pages, title, url, year, journal, issn, issue, isbn, publisher, volume);
        this.author = author1;
        this.doi = doi1;
        this.title = title1;
        this.url = url1;
        this.year = year1;
        this.publisher = publisher1;
    }

    /**
     * Creates citation in Chicago form.
     *
     * @return Citation of book in Chicago form.
     */
    public String ChicagoStyleBook() {
        String str = author + ". " + title + ". " + publisher + ", " + year + ", " + doi + ". " + url;
        return str;
    }

    /**
     * Creates citation in APA form.
     *
     * @return Citation of book in APA form.
     */
    public String APA7thStyleBook() {
        String str = author + ".  (" + year + "): " + title + ". " + publisher + ". " + doi + ".";
        return str;
    }

    /**
     * Creates citation in Harvard form.
     *
     * @return Citation of book in Harvard form.
     */
    public String HarvardStyleBook() {
        String str = author + ". (" + year + ")." + title + ". " + publisher + ".  Available at: " + url;
        return str;
    }

    /**
     * Creates citation in IEEE form.
     *
     * @return Citation of book in IEEE form.
     */
    public String IEEEStyleBook() {
        String str = author + ", " + title + ". " + publisher + ", " + year + ".";
        return str;
    }
}
