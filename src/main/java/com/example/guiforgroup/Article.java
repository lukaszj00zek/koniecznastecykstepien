package com.example.guiforgroup;

class Article extends Source {
    private String issn;
    private String issue;
    private String volume;

    //Article's constructor based on the Source
    public Article(String id, String author, String doi, String month, String pages, String title, String url, String year,
                   String journal, String issn, String issue, String isbn, String publisher, String volume) {
        super(id, author, doi, month, pages, title, url, year, journal, issn, issue, isbn, publisher, volume);
        this.issn = issn;
        this.issue = issue;
        this.volume = volume;
    }

    public String getIssn() {
        return issn;
    }

    public String getIssue() {
        return issue;
    }

    public String getVolume() {
        return volume;
    }


}