package com.example.guiforgroup;

class Source {
    private String id;
    private String author;
    private String doi;
    private String month;
    private String pages;
    private String title;
    private String url;
    private String year;
    private String journal;
    private String issn;
    private String issue;
    private String isbn;
    private String publisher;
    private String volume;

    //Source constructor - base for article and book
    public Source(String id, String author, String doi, String month, String pages, String title, String url, String year,
                  String journal, String issn, String issue, String isbn, String publisher, String volume) {
        this.id = id;
        this.author = author;
        this.doi = doi;
        this.month = month;
        this.pages = pages;
        this.title = title;
        this.url = url;
        this.year = year;
        this.journal = journal;
        this.issn = issn;
        this.issue = issue;
        this.isbn = isbn;
        this.publisher = publisher;
        this.volume = volume;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getDoi() {
        return doi;
    }

    public String getMonth() {
        return month;
    }

    public String getPages() {
        return pages;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getYear() {
        return year;
    }

    public String getJournal() {
        return journal;
    }

    public String getIssn() {
        return issn;
    }

    public String getIssue() {
        return issue;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        return "id:'" + id + '\'' + "\n" +
                "author: '" + author + '\'' + "\n" +
                "doi: '" + doi + '\'' + "\n" +
                "month: '" + month + '\'' + "\n" +
                "pages: '" + pages + '\'' + "\n" +
                "title: " + title + '\'' + "\n" +
                "url: '" + url + '\'' + "\n" +
                "year:'" + year + '\'' + "\n" +
                "journal: '" + journal + '\'' + "\n" +
                "issn: '" + issn + '\'' + "\n" +
                "issue: '" + issue + '\'' + "\n" +
                "isbn: '" + isbn + '\'' + "\n" +
                "publisher: '" + publisher + '\'' + "\n" +
                "volume: '" + volume + '\'' + "\n";
    }
}