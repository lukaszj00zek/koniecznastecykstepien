package com.example.guiforgroup;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;


public class Bibliography {
    public static void main(String[] args) throws IOException {
        //Connecting database in .bib to the programme
        String path = "data.bib";
        FileReader file = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(file);
        List<Source> bibliographyList = new ArrayList<>();

        try {
            String line;
            StringBuilder sourceBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                sourceBuilder.append(line);
                if (line.trim().endsWith("}")) {
                    String sourceString = sourceBuilder.toString();
                    bibliographyList.add(parseSource(sourceString));
                    sourceBuilder.setLength(0);
                }
            }
            file.close();
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("File not found!");
        }

        removeDuplicates(bibliographyList); //removing duplicates

        //comparison based on the natural ordering of strings, but it ignores the case differences
        Collections.sort(bibliographyList, Comparator.comparing(Source::getTitle, String.CASE_INSENSITIVE_ORDER));


        //prints the full database
        System.out.println("Sorted Bibliography:");
        for (Source source : bibliographyList) {
            System.out.println(source);
        }

    }

    /**
     * Transform a BibTex object into the object.
     *
     * @param sourceString of the source.
     * @return Source type of object.
     */
    private static Source parseSource(String sourceString) {
        String id = extractValue(sourceString, "id");
        String author = extractValue(sourceString, "author");
        String doi = extractValue(sourceString, "doi");
        String month = extractValue(sourceString, "month");
        String pages = extractValue(sourceString, "pages");
        String title = extractValue(sourceString, "title");
        String url = extractValue(sourceString, "url");
        String year = extractValue(sourceString, "year");
        String journal = extractValue(sourceString, "journal");
        String issn = extractValue(sourceString, "issn");
        String issue = extractValue(sourceString, "issue");
        String isbn = extractValue(sourceString, "isbn");
        String publisher = extractValue(sourceString, "publisher");
        String volume = extractValue(sourceString, "volume");

        return new Source(id, author, doi, month, pages, title, url, year, journal, issn, issue, isbn, publisher, volume);
    }


    //Extracting particular values of the sources in database
    private static String extractValue(String line, String field) {
        Pattern pattern = Pattern.compile(field + " = \\{([^\\}]+)\\}");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            String value = matcher.group(1).trim();
            return value.isEmpty() ? null : value;
        }
        return null;
    }

    /**
     * Deletes duplicates of the database
     *
     * @param bibliographyList in form of list of objects.
     * @return Database in form of list of objects without duplicates.
     **/
    public static void removeDuplicates(List<Source> bibliographyList) {
        Set<String> authors = new HashSet<>();
        Set<String> dois = new HashSet<>();
        Set<String> titles = new HashSet<>();


        List<Source> uniqueSources = new ArrayList<>();

        for (Source source : bibliographyList) {
            if (authors.add(source.getAuthor()) &&
                    dois.add(source.getDoi()) &&
                    titles.add(source.getTitle())) {

                uniqueSources.add(source);
            }
        }

        bibliographyList.clear();
        bibliographyList.addAll(uniqueSources);
    }
}
