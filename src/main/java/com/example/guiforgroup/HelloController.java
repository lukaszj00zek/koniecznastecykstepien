package com.example.guiforgroup;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class HelloController {

    @FXML
    private TextField GPTbar;

    @FXML
    private TextField OutputBar;

    @FXML
    private Button btnAPA;

    @FXML
    private Button btnArticle;

    @FXML
    private Button btnBook;

    @FXML
    private Button btnChicago;

    @FXML
    private Button btnCite;

    @FXML
    private Button btnCopy;

    @FXML
    private Button btnHarvard;

    @FXML
    private Button btnIEEE;

    @FXML
    private Button btnViewFullData;

    private boolean article;
    private boolean book;

    private boolean ieee;
    private boolean apa;
    private boolean chicago;
    private boolean harvard;

    private CitationMethods methods;


    @FXML
    void GPTbarAction(ActionEvent event) {

    }

    @FXML
    void OutputBar1(ActionEvent event) {

    }

    @FXML
    void btnAPAClicked(ActionEvent event) {
        apa = true;
        ieee = false;
        chicago = false;
        harvard = false;
    }

    @FXML
    void btnArticleClicked(ActionEvent event) {
        book = false;
        article = true;
    }

    @FXML
    void btnBookClicked(ActionEvent event) {
        book = true;
        article = false;
    }

    @FXML
    void btnChicagoClicked(ActionEvent event) {
        apa = false;
        ieee = false;
        chicago = true;
        harvard = false;
    }

    @FXML
    void btnCiteClicked(ActionEvent event) {
        if(book== true && apa == true){
            methods = CitationMethods.BookAPA;
        } else if (book== true && ieee == true) {
            methods = CitationMethods.BookIEEE;
        } else if (book== true && chicago == true) {
            methods = CitationMethods.BookChicago;
        } else if (book== true && harvard == true) {
            methods = CitationMethods.BookHarvard;
        } else if (article== true && apa == true) {
            methods = CitationMethods.ArticleAPA;
        } else if (book== true && ieee == true) {
            methods = CitationMethods.ArticleIEEE;
        } else if (book== true && chicago == true) {
            methods = CitationMethods.ArticleChicago;
        } else if (book== true && harvard == true) {
            methods = CitationMethods.ArticleHarvard;
        }


    }

    @FXML
    void btnCopyClicked(ActionEvent event) {
        String citation = OutputBar.getText();
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(citation);
        clipboard.setContent(content);
    }

    @FXML
    void btnHarvardClicked(ActionEvent event) {
        apa = false;
        ieee = false;
        chicago = false;
        harvard = true;
    }

    @FXML
    void btnIEEEClicked(ActionEvent event) {
        apa = false;
        ieee = true;
        chicago = false;
        harvard = false;
    }

    @FXML
    void btnViewFullDataClicked(ActionEvent event) {
        OutputBar.setText("See The Console");

    }

}
