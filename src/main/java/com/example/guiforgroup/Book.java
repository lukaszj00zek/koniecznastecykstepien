package com.example.guiforgroup;

class Book extends Source {
    private String isbn;

    //Book's constructor based on the Source
    public Book(String id, String author, String doi, String month, String pages, String title, String url, String year,
                String journal, String issn, String issue, String isbn, String publisher, String volume) {
        super(id, author, doi, month, pages, title, url, year, journal, issn, issue, isbn, publisher, volume);
        this.isbn = isbn;
    }

    public String getIsbn() {
        return isbn;
    }

}
