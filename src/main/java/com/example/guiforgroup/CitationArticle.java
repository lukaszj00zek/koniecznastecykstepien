package com.example.guiforgroup;

public class CitationArticle extends Article {
    protected String author;
    protected String doi;
    protected String month;
    protected String pages;
    protected String title;
    protected String url;
    protected String year;
    protected String journal;
    protected String issn;
    protected String issue;
    protected String volume;

    //Article's constructor based on the Source
    public CitationArticle(String id, String author, String doi, String month, String pages, String title, String url, String year, String journal, String issn,
                           String issue, String isbn, String publisher, String volume, String author1, String doi1, String month1, String pages1, String title1, String url1,
                           String year1, String journal1, String issn1, String issue1, String volume1) {
        super(id, author, doi, month, pages, title, url, year, journal, issn, issue, isbn, publisher, volume);
        this.author = author1;
        this.doi = doi1;
        this.month = month1;
        this.pages = pages1;
        this.title = title1;
        this.url = url1;
        this.year = year1;
        this.journal = journal1;
        this.issn = issn1;
        this.issue = issue1;
        this.volume = volume1;
    }

    /**
     * Creates citation in Chicago form.
     *
     * @return Citation of article in Chicago form.
     */

    public String ChicagoStyleArticle() {
        String str = author + ". \"" + title + ".\" " + journal + " " + volume + ", " + issn + ", " + issue + " (" + year + "): " + pages + ". " + doi + ". " + url;
        return str;
    }

    /**
     * Creates citation in APA form.
     *
     * @return Citation of article in APA form.
     */
    public String APA7thStyleArticle() {
        String str = author + " (" + year + "): " + journal + " " + volume + ", " + issn + ", " + issue + " (" + year + "): " + pages + ". " + doi + ". " + url;
        return str;
    }

    /**
     * Creates citation in Harvard form.
     *
     * @return Citation of article in Harvard form.
     */
    public String HarvardStyleArticle() {
        String str = author + ". (" + year + ")." + title + ". " + journal + ",  " + volume + " (" + issue + "), " + pages + ". Available at: " + url;
        return str;
    }

    /**
     * Creates citation in IEEE form.
     *
     * @return Citation of article in IEEE form.
     */
    public String IEEEStyleArticle() {
        String str = author + " \"" + title + ",\"" + journal + ", vol. " + volume + ", " + pages + ",  " + month + ", " + year;
        return str;
    }

}