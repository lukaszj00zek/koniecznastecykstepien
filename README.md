# Welcome to JustLooking!
### Version 0.1.4
JustLooking is a simple and pleasant helper in writing scientific papers by creating citation in the most popular styles, created by three students of medical informatics:
* Kamila Konieczna-Marjan;
* Łukasz Stecyk;
* Damian Stępień;

It supports four most popular citation styles:
* IEEE;
* APA;
* Chicago;
* Harvard;

It works and supports .bib BibTeX file which is a data source to the application.

### Working algorithm
The programme has few major algorithms which takes care of the .bib database, they do:
* deal with lack/addition of information;
* sort alphabetically the database;
* delete the duplicates of database;
* distinguishing articles from sources;
* create the proper citation for the sources;

### Major Methods

| Method name                                     | Class           | Input                                        | Output                                                  | Function                                    |
|-------------------------------------------------|-----------------|----------------------------------------------|---------------------------------------------------------|---------------------------------------------|
| parseSource(String sourceString)                | Bibliography    | SourceString of the source.                  | Source type of object.                                  | Transform a BibTex object into the object.  |
| removeDuplicates(List<Source> bibliographyList) | Bibliography    | bibliographyList in form of list of objects  | Database in form of list of objects without duplicates. | Deletes duplicates of the database          |
| HarvardStyleArticle()                           | CitationArticle | N/A                                          | Citation for article in Harvard style.                  | Creates citation in Harvard form.           |
| ChicagoStyleArticle()                           | CitationArticle | N/A                                          | Citation for article in Chicago style.                  | Creates citation in Chicago form.           |
| APA7thStyleArticle()                            | CitationArticle | N/A                                          | Citation for article in APA style.                      | Creates citation in APA form.               |
| IEEEStyleArticle()                              | CitationArticle | N/A                                          | Citation for article in IEEE style.                     | Creates citation in IEEE form.              |
| HarvardStyleBook()                              | CitationBook    | N/A                                          | Citation for book in Harvard style.                     | Creates citation in Harvard form.           |
| ChicagoStyleBook()                              | CitationBook    | N/A                                          | Citation for book in Chicago style.                     | Creates citation in Chicago form.           |
| APA7thStyleBook()                               | CitationBook    | N/A                                          | Citation for book in APA style.                         | Creates citation in APA form.               |
| IEEEStyleBook()                                 | CitationBook    | N/A                                          | Citation for book in IEEE style.                        | Creates citation in IEEE form.              |


### GUI
The programme has implemented GUI, which allows user to simply use the program. 

#### Header:
* Logo;
* Major choice of type of the source (book/article);

#### Body:
* Choice of type of citation (IEEE/APA/Chicago/Harvard);
* Search bar (which achieves more functionality in future updates);
* Cite button - makes the program work;
* Text field - the output field - uneditable for the user;

#### Footer:
* Copy button;
* Full database button - user can see the full database in form of console, alphabetically sorted and without duplicates;



